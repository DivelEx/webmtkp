<?php
$cust = [
  "cnum" => "2001",
  "cname" => "Hoffman",
  "city" => "London",
  "snum" => "1001"];

echo "Исходный массив:<br>";
foreach ($cust as $key => $value)
{
 echo "$key = $value <br>";
}

$custadd = [
  "rating" => "100"
];
$cust = $cust + $custadd;
echo "<br><br>Массив с новым элементом:<br>";
foreach ($cust as $key => $value)
{
 echo "$key = $value <br>";
}

echo "<br><br>Сортировка по значению:<br>";
asort($cust);
foreach ($cust as $key => $value)
{
 echo "$key = $value <br>";
}

echo "<br><br>Сортировка по ключу:<br>";
ksort($cust);
foreach ($cust as $key => $value)
{
 echo "$key = $value <br>";
}

echo "<br><br>Сортировка массива функцией sort():<br>";
sort($cust);
foreach ($cust as $key => $value)
{
 echo "$key = $value <br>";
}
echo ;
 ?>
