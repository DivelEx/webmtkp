 <?php

 $x = rand(3, 20);
 for ($i = 1; $i <= $x; $i++)
 {
   $arr[$i]=rand(10, 99);
 }
 echo "Массив из ".$x.
" элементов заполненный случайными числами:";
 foreach ($arr as $elem)
 {
   echo " $elem";
 }

 echo "<br><br>Отсортированный массив:";
 sort($arr);
 foreach ($arr as $elem)
 {
   echo " $elem";
 }

 echo "<br><br>Отсортированный на оборот:";
 $arr = array_reverse($arr);
 foreach ($arr as $elem)
 {
   echo " $elem";
 }

 echo "<br><br>Массив без последнего элемента:";
 $stack = array_pop($arr);
 foreach ($arr as $elem)
 {
   echo " $elem";
 }

 echo "<br><br>Кол-во элементов в массиве: ".count($arr);
 echo "<br><br>Сумма элементов в массиве: ".array_sum($arr);

 echo "<br><br>Среднее арифметическое
  элементов массива: ".array_sum($arr)/count($arr);

 if (in_array(50, $arr))
    echo "<br><br>Число 50 есть в массиве";
 else
    echo "<br><br>Числа 50 в массиве нету";

 echo "<br><br>Массив без повторяющихся элементов:";
 $arr = array_unique($arr);
 foreach ($arr as $elem)
 {
    echo " $elem";
 }
 ?>
