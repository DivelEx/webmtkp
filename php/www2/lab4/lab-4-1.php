 <?php
 echo "Треугольные числа  от 1 до 10:";
 for ($n = 1; $n <= 10; $n++)
 {
   $treug[$n - 1] = $n * ($n + 1) / 2;
 }
 foreach ($treug as $elem)
 {
   echo " $elem";
 }

 echo "<br><br>Массив квадратов натуральных чисел от 1 до 10:";
 for ($n = 1; $n <= 10; $n++)
 {
   $kvd[$n - 1]= $n * $n;
 }
 foreach ($kvd as $elem)
 {
   echo " $elem";
 }

 echo "<br><br>Слияние массивов:";
 $rez = array_merge($treug, $kvd);
 foreach ($rez as $elem)
 {
   echo " $elem";
 }

 echo "<br><br>Отсортированный слитый массив:";
 sort($rez);
 foreach ($rez as $elem)
 {
   echo " $elem";
 }

 echo "<br><br>Массив без первого элемента:";
 $stack[] = array_shift($rez);
 foreach ($rez as $elem)
 {
   echo " $elem";
 }

 echo "<br><br>Массив без повторяющихся элементов:";
 $rez1 = array_unique($rez);
 foreach ($rez1 as $elem)
 {
   echo " $elem";
 }
 ?>
