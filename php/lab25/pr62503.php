<html>
<head>
 <title>Десериализация объекта</title>
</head>
<body>
 <?php
 class A {
 public $one = 1;
 public function show_one() {
 echo $this->one;
 }
 }
 $s = file_get_contents('store');
 $a = unserialize($s);
 $a->show_one();
 ?>
</body>
</html>
