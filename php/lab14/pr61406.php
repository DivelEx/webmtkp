<html>
<head>
 <title>Функция, которая выводит таблицу умножения</title>
</head>
<body>
 <?php
 function getTable($cols = 10 , $rows = 10, $color = 'yellow')
 {
 echo "<table border='1' align='center'>";
 for($tr = 1; $tr <= $rows; $tr++)
 {
 echo "<tr>";
 for($td = 1; $td <= $cols; $td++)
 {
 if($tr == 1 or $td == 1)
 echo "<th style='background: $color'>".$tr * $td."</th>";
 else echo "<td>".$tr * $td."</td>";
 }
 echo "</tr>";
 }
 echo "</table>";
 }
 getTable(3, 3, 'red');
 getTable(4, 4, 'blue');
 getTable(5, 5, 'green');
 getTable();
?>
</body>
</html>
