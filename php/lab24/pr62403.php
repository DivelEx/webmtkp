<html>
<head>
 <title>Дескрипторы Private, Public и Protected</title>
</head>
<body>
 <?php
 class f
 {
 private $x;
 public function public_f()
 {
 echo "Это открытый метод.<br>";
 }
 protected function protected_f()
 {
 // Мы можем вызывать закрытые методы,
 // когда находимся в том же классе
 $this->private_f();
 echo "Это защищённый метод.<br>";
 }
 private function private_f()
 {
 $this->x = 3;
 echo "Это закрытый метод.<br>";
 }
 }
 class f2 extends f
 {
 public function display()
 {
 $this->protected_f();
 $this->public_f();
 }
 }
 $x = new f();
 $x->public_f();
 $x2 = new f2();
 $x2->display();
 ?>
</body>
</html>
