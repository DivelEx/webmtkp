<html>
<head>
 <title>Использование конструктора и деструктора</title>
</head>
<body>
 <?php
 class foo
 {
 var $x;
 // Конструктор
 function __construct($x)
 {
 $this->x = $x;
 }
 // Вывод значения x на экран
 function display()
 {
 echo $this->x."<br>";
 }
 // Деструктор
 function __destruct()
 {
 echo "Вызван деструктор";
 }
 }
 $a = new foo(4);
 $a->display();
 ?>
</body>
</html>
