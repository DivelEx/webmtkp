<html>
<head>
 <title>Использование ссылок</title>
</head>
<body>
 <?php
 class f
 {
 var $x;
 // Задать значение x
 function setX($x)
 {
 $this->x = $x;
 }
 // Вернуть значение x
 function getX()
 {
 return $this->x;
 }
 }
 $o1 = new f;
 $o1->setX(4);
 $o2 = $o1;
 $o1->setX(4);
 if($o1->getX() == $o2->getX())
 {
  echo "Значения равны!";
 }
 else
 {
    echo "Значения не равны!";
 }
 ?>
</body>
</html>
