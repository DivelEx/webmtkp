<html>
<head>
 <title>Выделение подстроки в строке</title>
</head>
<body>
 <?php
 $str1 = "фамилия, имя, отчество";
 $str2 = substr($str1, 9);
 echo "<B>Исходная строка: </B><BR>$str1<BR>";
 echo "<B>Подстрока: </B><BR>$str2<BR>";
 $str3 = substr($str1, -8);
 echo "<B>8 последних символов строки: </B><BR>$str3";
 ?>
</body>
</html>
