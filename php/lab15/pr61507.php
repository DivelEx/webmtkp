<html>
<head>
 <title>Сравнение строк</title>
</head>
<body>
 <?php
 $name1 = "Иван Петров.";
 $name2 = "Иван Петров.";
 echo "<B>Строка 1:</B> $name1<br>";
 echo "<B>Строка 2:</B> $name2<br>";
 $rez = strcmp($name1,$name2);
 if ($rez == 0) echo "Строки полностью равны";
elseif ($rez == -1) echo "Строка 1 меньше строки 2";
 else echo "Строка 1 больше строки 2";
 ?>
</body>
</html>
