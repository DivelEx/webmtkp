<html>
<head>
 <title>Поиск подстроки в строке</title>
</head>
<body>
 <?php
 $str = "Пример использования функции strstr.";
 $substr = "функции";
 echo "Исходная строка: $str<br>";
 echo 'Подстрока "';
 echo $substr;
 if (strstr($str, $substr))
 echo '" найдена в исходной строке.';
 else echo '" не найдена в исходной строке.';
 ?>
</body>
</html>
