<html>
<head><title>Таблица синусов и косиносов</title></head>
<body>
 <table align="center" border="1">
   <tr>
     <td colspan="18" align = "center">Таблица синусов и косиносов</td>
   </tr>
 <?php
 $step = M_PI / 16;

 echo "<tr>";
 echo "<td>"."      "."</td>";
 for ($i=0; $i < M_PI; $i = $i + $step)
 {
    echo "<td>". round($i, 3) ."</td>";
 }
 echo "<td>".(round(M_PI, 3))."</td>";
 echo "</tr>";

 echo "<tr>";
 echo "<td>"."cos(x)"."</td>";
 for ($i=0; $i < M_PI; $i = $i + $step)
 {
    echo "<td>".(round(cos($i), 3))."</td>";
 }
 echo "<td>".(round(cos(M_PI), 3))."</td>";
 echo "</tr>";

 echo "<tr>";
 echo "<td>"."sin(x)"."</td>";
 for ($i=0; $i < M_PI; $i = $i + $step)
 {
    echo "<td>".(round(sin($i), 3))."</td>";
 }
 echo "<td>".(round(sin(M_PI), 3))."</td>";
 echo "</tr>";
 ?>
</body>
</html>
