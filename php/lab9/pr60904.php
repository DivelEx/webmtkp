<html>
<head>
 <title>Применение условного оператора</title>
</head>
<body>
 <?php
 if (date("m") >= 3 && date("m") <= 5)
 {
 echo "<p>Сейчас весна!</p>";
 }
 elseif (date("m") >= 6 && date("m") <= 8)
 {
 echo "<p>Сейчас лето!</p>";
 }
 elseif (date("m") >= 9 && date("m") <= 11)
 {
 echo "<p>Сейчас осень!</p>";
 }
 else
 {
 echo "<p>Сейчас зима!</p>";
 }
 ?>
</body>
</html>
