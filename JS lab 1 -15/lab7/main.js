function ex1() {
    document.write('Данный текст был выведен на страницу с помощью JavaScript!');
}

function ex2(v1, v2) {
    return v1 + v2;
}

function ex3() {
    // Объявим глобальные переменные var1 и var2
    var var1 = "var1 существует";
    var var2;

    function func1() {
        // Присвоим var2 значение внутри функции func1
        var var2 = "var2 существует";
    }
    // Из другой функции выведем содержимое переменной var1 и var2 на страницу
    function func2() {
        // Выводим содержимое переменной var1
        document.write(var1 + '<br />');
        // Выводим содержимое переменной var2
        document.write(var2);
    }
    func2();
}