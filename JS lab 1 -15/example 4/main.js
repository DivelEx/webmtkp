﻿function n1() {
  var date = new Date();

  document.write("День: ", date.getDate());
  var months = [
    'янв', 'фев', 'мар', 'апр', 'май', 'июн',
    'июл', 'авг', 'сен', 'окт', 'ноя', 'дек'
  ];
  var month = date.getMonth();

  document.write("<br>Месяц: ", months[month]);

  document.write("<br>Год: ", date.getFullYear());

  var date = new Date();
  var day = date.getDate();
  if (day < 10) {
    day = 0 + String(day);
  }
  var month = date.getMonth() + 1;
  if (month < 10) {
    month = 0 + String(month);
  }
  var year = date.getFullYear();
  document.write("<br>Дата: ", date.getHours(), ":", date.getMinutes(), ":", date.getSeconds(), " ", day, ".", month, ".", year);
}

function n2() {
  var date = new Date();
  var day = date.getDay();
  var days = ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'];
  document.write("<br><br>Номер дня: ", day);
  document.write("<br>День недели: ", days[day]);
  var date = new Date(2015, 7, 0);
  var day = date.getDay();
  document.write("<br>День недели 07-01-2015 : ", days[day]);
}

function n3() {
  var now = new Date();
  var date = new Date(1970, 0, 1);
  var diff = now.getTime() - date.getTime();
  diff = (diff / 1000) / 60;
  document.write("<br><br>Количество минут с 1970 года: ", diff);

}

function n4() {

  var now = new Date();
  var date = Date.parse('1988-03-01');
  var diff = now.getTime() - date;
  diff = ((diff / 1000) / 60) / 60;
  document.write("<br><br>Количество часов с марта 1988 года: ", diff);

}

function n5() {
  var now = new Date();
  var month = now.getMonth();
  var date = new Date(now.getFullYear(), month, now.getDate());
  var diff = now.getTime() - date.getTime();
  diff = (diff / 1000);
  document.write("<br><br>Количество секунд с начала дня: ", diff);

}

function n6() {
  var now = new Date();
  var month = now.getMonth();
  var date = new Date(now.getFullYear(), month, now.getDate(), 23, 59, 59);
  var diff = date.getTime() - now.getTime();
  diff = (diff / 1000);
  //document.write("<br>Дата: ",date.getHours(),":",date.getMinutes(),":",date.getSeconds(),date.getFullYear(), date.getMonth(), date.getDate());
  //document.write("<br>Дата: ",now.getHours(),":",now.getMinutes(),":",now.getSeconds(),now.getFullYear(), now.getMonth(), now.getDate());
  document.write("<br><br>Количество секунд до конца дня: ", diff);

}

function n7(inp) {
  var yearP = inp.slice(0, 4);
  var monthP = inp.slice(5, 7);
  var dayP = inp.slice(8, 10);
 
  var now = new Date();
  var day = now.getDate();

  var month = now.getMonth() + 1;

  var year = now.getFullYear();

  now.setHours(0, 0, 0, 0);

  var date = new Date(year, (monthP - 1), dayP);

  if (monthP > (month + 1)) {
    console.log("1");
    var birthday = date.getTime() - now.getTime();
  } else if (monthP == (month + 1)) {
    console.log("2");

    if (dayP > (day + 1)) {
      var birthday = date.getTime() - now.getTime();
    } else if (dayP == (day + 1)) {
      // 0 day
      var birthday = 0;
    } else {
      date.setFullYear((year + 1));
      var birthday = date.getTime() - now.getTime();
    }
  } else {
    console.log("3");
    date.setFullYear((year + 1));
    var birthday = date.getTime() - now.getTime();
  }

  // document.write("<br>Дата: ",now.getHours(),":",now.getMinutes(),":",now.getSeconds(),now.getFullYear(), now.getMonth(), now.getDate());

  birthday = (((birthday / 1000) / 60) / 60) / 24 - 1;


  var elem = document.getElementById('brth'); //получаем наш абзац
  elem.innerHTML = '<br><br> День рождение через: ' + birthday; //записываем в него новый текст
}