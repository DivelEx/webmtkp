function task17() {
    var input = document.getElementById('tsk17Input');
    input.value++;
}

function task18() {
    var input = document.getElementById('tsk18Input');
    var button = document.getElementById('tsk18Button');

    button.addEventListener('click', function func() {
        let arr = input.value.split(',');

        // document.writeln("<ul>");

        for (var elem of arr) {
            document.write("<li>" + elem + "</li>");
        }
        // document.writeln("</ul>");
    });
}

function task19() {
    var input = document.getElementById('tsk19Input');
    var p = document.getElementById('tsk19P');
    var button = document.getElementById('tsk19Button');

    button.addEventListener('click', function func() {
        p.innerHTML += input.value + ', ';
    });

    input.addEventListener('focus', function () {
        input.value = '';
    });
    document.addEventListener('keydown', function (event) {
        if (event.code == 'Enter') {
            p.innerHTML += input.value + ', ';
        }
    });
}

function task20() {
    let divs = document.querySelectorAll('.text');

    for (var div of divs) {
        document.write(div.innerHTML.slice(0, 10).concat('...'));
    }
}

function task21() {
    var arr = [1, 2, 3, 4, 5];
    var div = document.getElementById('tsk21Div');
    var sum = 0;

    for (var i = 0; i < arr.length; i++) {
        document.writeln('<input type="text"  value="' + arr[i] + '"><br>');
        sum += arr[i];
    }
}

function task22() {
    var x = document.getElementById('tsk22Input');
    var num = x.value;

    if (num >= 1 && num <= 100) {
        x.style.background = 'green';
    } else {
        x.style.background = 'red';
    }
}

function task23() {
    var x = document.getElementById('tsk23Input');
    var allСharacters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()_+=-?:№<>/|{}[]`~';
    var str = '';

    function getRandom(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    for (var i = 0; i < 8; i++) {
        str += allСharacters[getRandom(0, allСharacters.length)];
    }
    x.value = str;
}

function task24() {
    var x = document.getElementById('tsk24Input');
    var str = x.value;
    var newStr = '';

    function getRandom(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    while (newStr.length < str.length) {
        newStr += str[getRandom(0, str.length - 1)];
    }
    x.value = newStr;
}

function task25() {
    var x = document.querySelector('#tsk25checkbox');
    if (x.checked) {
        x.checked = false;
    } else {
        x.checked = true;
    }
}

function task26() {
    var x = document.getElementById("tsk26Input").value;
    var p = document.getElementById('tsk26P');
    var f = (x - 32) * (5 / 9)
    p.innerHTML = f;
}

function task27() {
    var x = document.getElementById("tsk27Input").value;
    var f = 1;
    for (var i = 1; i <= x; i++) {
        f *= i;
    }
    var p = document.getElementById('tsk27P');
    p.innerHTML = f;
}

function task28() {
    var a = document.getElementById("tsk28Input1").value;
    var b = document.getElementById("tsk28Input2").value;
    var c = document.getElementById("tsk28Input3").value;

    var d = b * b - 4 * a * c;

    if (d < 0) {
        string = "Так как дискриминант меньше нуля, то уравнение не имеет действительных решений.";
    } else if (d > 0) {
        string = "Два корня: <br>x<sub>1</sub> = ";
        string += (-b - Math.sqrt(d)) / (2 * a);
        string += "<br>x<sub>2</sub> = "
        string += (-b + Math.sqrt(d)) / (2 * a);
    } else if (d === 0) {
        string = "Один корен: <br>x = ";
        string += -b / (2 * a);
    }

    var p = document.getElementById('tsk28P');
    p.innerHTML = string;
}