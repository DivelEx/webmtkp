﻿function func1() {
    var elem = document.getElementById('test1'); //получаем наш абзац
    elem.innerHTML = 'Ку-ку! А я <b>жирный</b>'; //записываем в него новый текст
}

function func2() {
    var elem = document.getElementById('test2');
    elem.outerHTML = '<h3>Абзац превратился в h3</h3>';
}

function func3() {
    var elems = document.getElementsByTagName('p');
    elems[2].innerHTML = '<h2>' + 'А я в h2 не поменяюсь!' + '</h2>';
    elems[3].innerHTML = '<h2>' + 'А я в h2 не поменяюсь!' + '</h2>';
    elems[4].innerHTML = '<h2>' + 'А я в h2 не поменяюсь!' + '</h2>';
    elems[5].innerHTML = '<h2>' + 'А я в h2 не поменяюсь!' + '</h2>';
}

function func4() {
    var elems = document.getElementsByClassName('www');
    for (var i = 0; i < elems.length; i++) {
        elems[i].innerHTML = i + 1;
    }
}

function func5() {
    var elems = document.querySelectorAll('p.zzz');
    for (var i = 0; i < elems.length; i++) {
        elems[i].innerHTML = i + 1;
    }
}