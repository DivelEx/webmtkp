function ex1() {
    // Создаем массив.
    balls = {
        "гольф": "Мячи для гольфа, 6 шт.",
        "теннис": "Мячи для тенниса, 3 шт.",
        "футбол": "Футбольный мяч, 1 шт.",
        "пинг-понг": "Мячи для пинг-понга, 12 шт."
    };
    // Выводим массив в окно браузера.
    for (ball in balls)
        document.write(ball + " = " + balls[ball] + "<br>");
}

function ex2() {
    // Создаем массив.
    checkerboard = Array(
        Array(' ', 'o', ' ', 'o', ' ', 'o', ' ', 'o'),
        Array('o', ' ', 'o', ' ', 'o', ' ', 'o', ' '),
        Array(' ', 'o', ' ', 'o', ' ', 'o', ' ', 'o'),
        Array(' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
        Array(' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
        Array('O', ' ', 'O', ' ', 'O', ' ', 'O', ' '),
        Array(' ', 'O', ' ', 'O', ' ', 'O', ' ', 'O'),
        Array('O', ' ', 'O', ' ', 'O', ' ', 'O', ' '));
    // Выводим массив в окно браузера.
    document.write("<pre>");
    for (j = 0; j < 8; ++j) {
        for (k = 0; k < 8; ++k)
            document.write(checkerboard[j][k] + " ");
        document.write("<br>");
    }
    document.write("</pre>");
}

function ex3() {
    let l = 10;

    let arr = [];

    for (let i = 0; i < l; i++) {
        arr.push(Math.random());
        document.write(arr[i] + '<br>');
    }
    var largest = Math.max.apply(Math, arr);
    document.write('Наибольшее число массива: ' + largest);
}

function ex4() {
    let l = 20;

    let arr = [];

    function randomInteger(min, max) {
        let rand = min + Math.random() * (max + 1 - min);
        return Math.floor(rand);
    }

    for (let i = 0; i < l; i++) {
        arr.push(randomInteger(1, 999));
        //document.write(arr[i] + '<br>');
    }


    arr = arr.sort();

    for (let i = 0; i < arr.length - 1; i++) {
        document.write(arr[i] + '<br>');
    }

    let arrsum = 0;

    for (let i = 0; i < arr.length; i++) {
        arrsum += arr[i];
    }

    let res = arrsum / arr.length;

    document.write('<br>' + 'Среднее арифместическое массива: ' + arrsum);
}

function ex5(k) {

    let f1 = 0,
        f2 = 1,
        cf = 1;

    document.write('0' + ' ');
    document.write('1' + ' ');

    for (let i = 1; i < k - 1; i++) {
        cf = f1 + f2;
        f1 = f2;
        f2 = cf;
        document.write(cf + ' ');
    }
}