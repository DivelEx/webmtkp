function ex1() {
    for (i = 0; i < 255; i += 16) {
        document.write('<TR>');
        for (j = 0; j < 255; j += 16) {
            document.write('<TD BGCOLOR=rgb(0,', i, ',', j, ')>&nbsp; </TD>');
        }
        document.writeln('</TR>');
    }
}

function ex2() {
    var i, j; // счетчик строк и счетчик столбцов
        const N = 10; // размер таблицы
        // Начинаем строить таблицу (с толщиной границы равной 1)
        document.write("<table border=" + 1 + ">");
        for (i = 1; i <= N; i++) {
            // Строим очередную строку таблицы
            document.write("<tr align=right>");
            for (j = 1; j <= N; j++) {
                // Каждый элемент таблицы заносим в отдельную ячейку.
                // Если номер столбца и номер ряда совпадают,
                // то используем жирный шрифт.
                if (i === j) document.write("<td><b>" + i * j + "</b></td>");
                else document.write("<td>" + i * j + "</td>");
            }
            document.write("</tr>");
        }
        document.write("</table>");
}