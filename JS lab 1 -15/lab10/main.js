function ex1() {
    document.write(screen.width);
    document.write(" - полная ширина экрана в пикселах.<br>");
    document.write(screen.height);
    document.write(" - полная высота экрана в пикселах.<br>");
    document.write(screen.availWidth);
    document.write(
        " - ширина, доступная для окна браузера.<br>");
    document.write(screen.availHeight);
    document.write(
        " - высота, доступная для окна браузера.<br>");
    document.write(screen.colorDepth + " - глубина цвета.");
}

function ex2() {
    for (var prop in navigator) {
        document.write(prop + ": " + navigator[prop] + "<br>");
    }
}

function ex3() {
    document.write(navigator.appName);
    document.write(" - имя приложения.<br>");
    document.write(navigator.appCodeName);
    document.write(" - кодовое имя браузера.<br>");
    document.write(navigator.cpuClass);
    document.write(" - класс процессора компьютера.<br>");
    document.write(navigator.platform);
    document.write(" - название клиентской платформы.<br>");
    document.write(navigator.systemLanguage);
    document.write(" - код языка операционной системы.<br>");
    document.write(navigator.browserLanguage);
    document.write(" - код языка браузера.<br>");
    document.write(navigator.userLanguage);
    document.write(" - код языка пользователя.<br>");
    if (navigator.onLine) {
        document.write("Клиент подключен к Интернет.");
    } else {
        306
        document.write("Клиент отключен от Интернет.");
    }
    document.write("<br>");
    if (navigator.cookieEnabled)
        document.write("Прием cookie разрешен.");
    else document.write("Прием cookie запрещен.");
}

function ex4() {
    document.write(window.location.href);
    document.write(" - полный URL-адрес документа.<br>");
    document.write(window.location.protocol);
    document.write(" - идентификатор протокола.<br>");
    document.write(window.location.port);
    document.write(" - номер порта.<br>");
    document.write(window.location.host);
    document.write(" - имя компьютера в сети Интернет, ");
    document.write("вместе с номером порта.<br>");
    document.write(window.location.hostname);
    document.write(" - имя компьютера в сети Интернет.<br>");
    document.write(window.location.pathname);
    document.write(" - путь и имя файла.<br>");
    document.write(window.location.search);
    document.write(" - строка параметров.<br>");
}