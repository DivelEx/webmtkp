
        // Дождаться, когда загрузится окно.
        window.addEventListener('load', initialize, false);

        function initialize() {
            // Переместить квадрат после щелчка на нем.
            document.getElementById("square").
            addEventListener('click', function (e) {
                // Получить начальную позицию.
                var left = window.getComputedStyle(e.target).
                getPropertyValue("left");
                // Преобразовать left в десятичное целое число
                left = parseInt(left, 10);
                moveSquare(left, 500);
            }, false);
        }

        function moveSquare(left, numMoves) {
            document.getElementById("square").style.left = left + "px";
            if (numMoves > 0) {
                numMoves--;
                left++;
                setTimeout(moveSquare, 10, left, numMoves);
            } else {
                return;
            }
        }
    