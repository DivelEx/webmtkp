function ex1() {
    alert('Привет!');
}

function ex2() {
    alert('Привет!');
}

function ex3() {
    alert('Привет!');
}

function ex4() {
    alert('Привет!');
}

function ex5(elem) {
    alert(elem.value);
}

function ex6(elem) {
    elem.value = 'О, теперь на меня больше не нажать!';
    elem.disabled = true;
}

function ex7(){
    elem = document.getElementById('test');
    elem.style.color = 'red';
    elem.style.width = '300px';
}
