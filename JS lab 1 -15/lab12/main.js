function growImage(imgToGrow) {
    imgToGrow.width += imgToGrow.width * .05;
    imgToGrow.height += imgToGrow.width * .05;
}

function restoreImage(imgToShrink) {
    imgToShrink.width = imgToShrink.naturalWidth;
    imgToShrink.height = imgToShrink.naturalHeight;
}


var slides = [
    "<div id='slidel'>Первый слайд:<br><img src='image1.jpg'></div>",
    "<div id='slide2'>Второй слайд:<br><img src='image2.jpg'></div>",
    "<div id='slide3'>Третий слайд:<br><img src='image3.jpg'></div>"
];
var currentSlide = 0;
var numberOfSlides = slides.length - 1;
window.addEventListener("load", loader, false);

function loader() {
    changeImage();
}

function changeImage() {
    if (currentSlide > numberOfSlides) {
        currentSlide = 0;
    }
    document.getElementById("carousel").innerHTML = slides[currentSlide];
    currentSlide++;
    setTimeout(changeImage, 1000);
}