function n1() {

  var arr = [];
  arr[0] = 'x';
  for (var i = 1; i < 10; i++) {
    arr[i] = arr[i - 1] + 'x';
  }
  console.log(arr);
  document.write("<br>Массив с x: ", arr);
}

function n2() {
  var arr = [];
  for (var i = 0; i < 10; i++) {

    arr[i] = i + 1;
    for (var j = 0; j <= (i - 1); j++) {
      arr[i] = String(arr[i]) + String(i + 1);
    }
  }
  console.log(arr);
  document.write("<br>Массив с числами: ", arr);
}

function n3() {
  var arr = [1, 5, 2, 3, 4, 5, 1];
  var number = 0;
  var sum = 0;

  for (var i = 0; i <= arr.length; i++) {
    if (sum <= 10) {
      number += 1;
      sum += arr[i];
    }
  }
  document.write("<br>Массив: ", arr);
  document.write("<br>Количество элементов с начала массива которое надо сложить чтобы в сумме получилось больше 10: ", number);
}

function n4() {
  var arr = [5, 6, 8, 12, 3];
  var result = [];
  for (var i = arr.length - 1; i >= 0; i--) {
    result.push(arr[i]);
  }
  document.write("<br>Массив: ", arr);
  document.write("<br>Перевёрнутый массив: ", result);
}

function n5() {
  var arr = [
    [1, 2, 3],
    [4, 5],
    [6]
  ];
  var sum = 0;
  for (var i = 0; i < arr.length; i++) {
    for (var j = 0; j < arr[i].length; j++) {
      sum += arr[i][j];
    }
  }
  console.log(sum);
  document.write("<br>Сумма элементов массива: ", sum);
}

function n6() {
  var arr = [
    [
      [1, 2],
      [3, 4]
    ],
    [
      [5, 6],
      [7, 8]
    ]
  ];
  var sum = 0;
  for (var i = 0; i < arr.length; i++) {
    for (var j = 0; j < arr[i].length; j++) {
      for (var h = 0; h < arr[i].length; h++) {
        sum += arr[i][j][h];
      }
    }
  }
  console.log(sum);
  document.write("<br>Сумма элементов массива: ", sum);
}