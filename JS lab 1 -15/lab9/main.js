function ex1(form) {
    if (confirm("Вы уверены?"))
        form.result.value = eval(form.expr.value)
    else alert("Повторите ввод!")
}

var line = "Пример бегущей строки.";
var speed = 200;
var i = 0;

function ex2() {
    if (i++ < line.length) {
        document.form.ctc.value = line.substring(0, i);
    } else {
        document.form.ctc.value = " ";
        i = 0;
    }
    setTimeout('ex2()', speed);
}