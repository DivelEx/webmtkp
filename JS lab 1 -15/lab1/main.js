function ex1() {
    document.write("А это JavaScript!");
}

function ex2() {
    alert('Привет, Мир!');
}

function ex3() {
    document.write("Hello World!");
}

function ex4() {
    var a = 8;
    h = 10;
    document.write("Площадь треугольника равна ", a * h / 2, ".");
}

function ex5() {

    function care(a, h) {
        return a * h / 2;
    }

    let a1 = 4;
    let h1 = 16;
    var s = care(a1, h1);
    document.write("Площадь треугольника ", s, ".");
}

function ex6() {
    var name = prompt("Ваше имя?", "");
    alert("Пользователь: " + name);
}