function ex1() {
    for (i = 6; i > 0; i--) {
        document.write('<H', i,
            '>Это строка генерируется JS</H', i, '>');
    }
}

function ex2() {
    var i = 0;
    document.write('<H1>Квадраты целых чисел:</H1>');
    while (i < 10) {
        document.write(i, '<SUP>2</SUP>=', i * i, '<BR>');
        i++;
    }
}

function ex3() {
    var k = 0;
    for (var i = 0; i < 10; i++) {
        if (k == 0) {
            document.write('<H1 STYLE="color: red">Красный</H1>');
            k = 1;
        } else {
            document.write('<H1 STYLE="color: green">Зеленый</H1>');
            k = 0;
        }
    }
}

function ex4() {
    var i;
    const step = Math.PI / 16;

    document.write("<table align=" + "center" + " border=" + 1 + ">");
    document.write("<tr> <td colspan='18' align='center'>Таблица синусов и косиносов</td></tr>");
    document.write("<tr>");
    document.write("<td>" + "      " + "</td>");

    for (i = 0; i <= Math.PI; i += step) {
        document.write("<td>" + i.toFixed(3) + "</td>");
    }

    document.write("<td>" + Math.PI.toFixed(3) + "</td>" + "</tr>");
    document.write("<tr>" + "<td>" + "cos(x)" + "</td>");

    for (i = 0; i < Math.PI; i += step) {
        document.write("<td>" + Math.cos(i).toFixed(3) + "</td>");
    }

    document.write("<td>" + Math.cos(Math.PI).toFixed(3) + "</td>" + "</tr>");
    document.write("<tr>" + "<td>" + "sin(x)" + "</td>");

    for (i = 0; i < Math.PI; i += step) {
        document.write("<td>" + Math.sin(i).toFixed(3) + "</td>");
    }

    document.write("<td>" + Math.sin(Math.PI).toFixed(3) + "</td>" + "</tr>");

    document.write("</table>");
}

function ex5(a) {
    document.write("Модуль числа равен: ", Math.abs(a));
}

function ex6(a) {
    var f = 1;
    var a = a;

    for (var i = 1; i <= a; i++) {
        f *= i;
    }
    const answer = 'Факториал числа равен: ' + f;
    document.getElementById("fct").innerHTML = answer;
}