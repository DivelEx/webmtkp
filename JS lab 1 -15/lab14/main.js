 function ex1() {
     var mycanvas = document.getElementById("canvasld1");
     var mycontext = mycanvas.getContext('2d');
     mycontext.fillStyle = 'rgb(0,0,255)';
     mycontext.fillRect(30, 30, 300, 300);
     mycontext.fillStyle = 'rgba(0,255,0,0.5)';
     mycontext.fillRect(60, 60, 300, 300);
     mycontext.fillStyle = 'rgba(255,0,0,0.25)';
     mycontext.fillRect(90, 90, 300, 300);
 }

 function ex2() {
     var c = document.getElementById("canvasld2");
     var ctx = c.getContext("2d");
     var my_gradient = ctx.createLinearGradient(0, 0, 170, 0);
     my_gradient.addColorStop(0, "black");
     my_gradient.addColorStop(1, "white");
     ctx.fillStyle = my_gradient;
     ctx.fillRect(20, 20, 150, 100);
 }

 function ex3() {
     var mycanvas = document.getElementById("canvasld3");
     var mycontext = mycanvas.getContext('2d');
     var mygrad = mycontext.createLinearGradient(30, 30, 300, 300);
     mygrad.addColorStop(0, "#FF0000");
     mygrad.addColorStop(1, "#00FF00");
     mycontext.fillStyle = mygrad;
     mycontext.fillRect(0, 0, 400, 400);
 }

 function ex4() {
     var mycanvas = document.getElementById("canvasld4");
     var mycontext = mycanvas.getContext('2d');
     var mygradient =
         mycontext.createRadialGradient(300, 300, 0, 300, 300, 300);
     mygradient.addColorStop("0", "magenta");
     mygradient.addColorStop(".25", "blue");
     mygradient.addColorStop(".50", "green");
     mygradient.addColorStop(".75", "yellow");
     mygradient.addColorStop("1.0", "red");
     mycontext.fillStyle = mygradient;
     mycontext.fillRect(0, 0, 400, 400);
 }

 function ex5() {
     var cont1 =
         document.getElementById("Canv1").getContext('2d');
     var cont2 =
         document.getElementById("Canv2").getContext('2d');
     var cont3 =
         document.getElementById("Canv3").getContext('2d');
     var cont4 =
         document.getElementById("Canv4").getContext('2d');
     var gr1 = cont1.createLinearGradient(30, 30, 90, 90);
     gr1.addColorStop(0, "#FF0000");
     gr1.addColorStop(1, "#00FF00");
     cont1.fillStyle = gr1;
     cont1.fillRect(0, 0, 100, 100);
     var gr2 = cont2.createLinearGradient(30, 30, 90, 90);
     gr2.addColorStop(1, "#FF0000");
     gr2.addColorStop(0, "#00FF00");
     cont2.fillStyle = gr2;
     cont2.fillRect(0, 0, 100, 100);
     var gr3 = cont3.createLinearGradient(30, 30, 90, 90);
     gr3.addColorStop(0, "#0000FF");
     gr3.addColorStop(.5, "#00FFDD");
     cont3.fillStyle = gr3;
     cont3.fillRect(0, 0, 100, 100);
     var gr4 = cont4.createLinearGradient(30, 30, 90, 90);
     gr4.addColorStop(0, "#DD33CC");
     gr4.addColorStop(1, "#EEEEEE");
     cont4.fillStyle = gr4;
     cont4.fillRect(0, 0, 100, 100);
 }

 function ex6() {
     var canv = document.getElementById('canvasld6');
     var g = canv.getContext('2d');
     // Координаты цента большей окружности
     var x0 = 320,
         y0 = 240;
     // и её радиус
     var r = 200;
     grad = g.createRadialGradient(x0, y0, r, 220, 140, 2);
     grad.addColorStop(0, 'black');
     grad.addColorStop(1, 'yellow');
     g.fillStyle = grad;
     g.arc(x0, y0, r, 0, Math.PI * 2, true);
     g.fill();
 }

 function ex7() {
     var canv = document.getElementById('canvasld7');
     var g = canv.getContext('2d');
     // координата красной линии
     var y = 40;
     g.strokeStyle = 'black';
     g.lineWidth = 30;
     // Стиль miter
     g.beginPath();
     g.moveTo(25, 170);
     g.lineTo(25, y);
     g.lineTo(100, 120);
     g.stroke();
     // Стиль round
     g.beginPath();
     g.lineJoin = 'round'
     g.moveTo(150, 170);
     g.lineTo(150, y);
     g.lineTo(225, 120);
     g.stroke();
     // Стиль bevel
     g.beginPath();
     g.lineJoin = 'bevel'
     g.moveTo(275, 170);
     g.lineTo(275, y);
     g.lineTo(340, 120);
     g.stroke();
     //Красная линия
     g.beginPath();
     g.strokeStyle = 'red';
     g.lineWidth = 6;
     g.moveTo(5, y);
     g.lineTo(370, y);
     g.stroke();
 }