function ex1() {
    // Создаем массив.
    sports = ["Футбол", "Теннис", "Бейсбол"];
    document.write("Изначально = " + sports + "<br>");
    // Добавляем элемент.
    sports.push("Хоккей");
    document.write("После вставки = " + sports + "<br>");
    // Удаляем элемент.
    removed = sports.pop();
    document.write("После удаления = " + sports + "<br>")
    document.write("Удаленный элемент = " + removed + "<br>")
}

function ex2() {
    // Создаем массив.
    sports = ["Футбол", "Теннис", "Бейсбол", "Хоккей"]
    // Сортировка по алфавиту.
    sports.sort()
    document.write(sports + "<br>")
    // Сортировка по алфавиту в обратном порядке.
    sports.sort().reverse()
    document.write(sports + "<br>");
    // Создаем массив.
    numbers = [7, 23, 6, 74];
    // Сортировка чисел по возрастанию
    numbers.sort(function (a, b) {
        return a - b
    });
    document.write(numbers + "<br>");
    // Сортировка чисел по убыванию
    numbers.sort(function (a, b) {
        return b - a
    });
    document.write(numbers + "<br>");
}

function ex3() {
    sports = ["Футбол", "Теннис", "Бейсбол", "Хоккей"]
    sports.sort().reverse()
    document.write(sports + "<br>");
}