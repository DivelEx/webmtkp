function ex1() {
    // Обращение к canvas
    var canvas = document.getElementById("canvasld1");
    // Обращение к контексту
    var ctx = canvas.getContext("2d");
    ctx.moveTo(50, 70); // Перемещение курсора
    ctx.lineTo(150, 20); // Создание линии
    ctx.stroke(); // Отображение линии
}

function ex2() {
    var canvas = document.getElementById("canvasld2");
    var ctx = canvas.getContext("2d");
    ctx.strokeText("Зигзаг!!!!!!", 10, 10); // Написать текст
    ctx.stroke(); // Отобразить текст
    ctx.lineWidth = 10; // Установить ширину линии
    ctx.strokeStyle = "#0000FF"; // Установить цвет линии
    var x = [40, 70, 100, 130, 160, 190];
    var y = 80;
    ctx.moveTo(10, 80); // Переместить курсор
    ctx.beginPath(); // в начало пути
    for (var i = 0; i < x.length; i++) {
        if (y > 40) y = 40;
        else y = 80;
        ctx.lineTo(x[i], y);
    }
    ctx.stroke(); // Отобразить ломанную линию
}

function ex3() {
    var canvas = document.getElementById("canvasld3");
    var ctx = canvas.getContext("2d");
    ctx.strokeStyle = "green"; // Установить цвет линии
    ctx.fillStyle = "blue"; // Установить цвет заливки
    ctx.shadowBlur = 10; // Установить размер тени
    ctx.shadowColor = "brown"; // Установить цвет тени
    337
    ctx.rect(10, 30, 80, 40); // Рисование прямоугольника
    ctx.fill(); // Заполнить цветом
    ctx.stroke(); // Отобразить контур
}

function ex4() {
        var canvas = document.getElementById("canvasld4");
        var context = canvas.getContext("2d");
        // Голубая линия с закругленными концами
        context.beginPath();
        context.moveTo(50, 50);
        context.lineTo(300, 50);
        context.lineWidth = 10;
        context.strokeStyle = "#0000FF";
        context.lineCap = "round";
        context.stroke();
        // Зеленая линия с квадратными концами
        context.beginPath();
        context.moveTo(50, 100);
        context.lineTo(300, 100);
        context.lineWidth = 20;
        context.strokeStyle = "#00FF00";
        context.lineCap = "square";
        context.stroke();
        // Пурпурная линия с квадратными концами
        context.beginPath();
        context.moveTo(50, 150);
        context.lineTo(300, 150);
        context.lineWidth = 30;
        context.strokeStyle = "#FF00FF";
        context.lineCap = "butt";
        context.stroke();
}

function ex5() {
    var canvas = document.getElementById('canvasld5');
    var context = canvas.getContext('2d');
    context.fillRect(25, 25, 50, 50);
    context.clearRect(35, 35, 30, 30);
    context.strokeRect(100, 100, 50, 50);
}

function ex6() {
    function drawArc() {
        var canvas = document.getElementById("canvasld6");
        var context = canvas.getContext("2d");
        var centerX = 100;
        var centerY = 160;
        var radius = 75;
        var startingAngle = 1.1 * Math.PI;
        var endingAngle = 1.9 * Math.PI;
        var counterclockwise = false;
        context.arc(centerX, centerY, radius, startingAngle,
            endingAngle, counterclockwise);
        context.lineWidth = 10;
        context.strokeStyle = "black";
        context.stroke();
    };

    drawArc();

    function drawQuadratic() {
        var canvas = document.getElementById("canvasld6");
        var context = canvas.getContext("2d");
        context.moveTo(200, 150);
        var controlX = 288;
        var controlY = 0;
        var endX = 388;
        var endY = 150;
        context.quadraticCurveTo(controlX, controlY, endX, endY);
        context.lineWidth = 10;
        context.strokeStyle = "black";
        context.stroke();
    };

    drawQuadratic();

    function drawBezier() {
        var canvas = document.getElementById("canvasld6");
        var context = canvas.getContext("2d");
        context.moveTo(350, 350);
        var controlX1 = 440;
        var controlY1 = 10;
        var controlX2 = 550;
        var controlY2 = 10;
        var endX = 500;
        var endY = 150;
        context.bezierCurveTo(controlX1, controlY1, controlX2,
            controlY2, endX, endY);
        context.lineWidth = 10;
        context.strokeStyle = "black";
        context.stroke();
    };

    drawBezier();

    function drawCircle() {
        var canvas = document.getElementById("canvasld6");
        var context = canvas.getContext("2d");
        var centerX = 450;
        var centerY = 375;
        var radius = 70;
        context.beginPath();
        context.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
        context.fillStyle = "#800000";
        context.fill();
        context.lineWidth = 5;
        context.strokeStyle = "black";
        context.stroke();
    };

    drawCircle();

    function drawSemicircle() {
        var canvas = document.getElementById("canvasld6");
        var context = canvas.getContext("2d");
        var centerX = 100;
        var centerY = 375;
        var radius = 70;
        var lineWidth = 5;
        context.beginPath();
        context.arc(centerX, centerY, radius, 0, Math.PI, false);
        context.closePath();
        context.lineWidth = lineWidth;
        context.fillStyle = "#900000";
        context.fill();
        context.strokeStyle = "black";
        context.stroke();
    };

    drawSemicircle();
}