function ex1(obj) {
    var a = Array(6);
    a[0] = 1 * obj.num1.value;
    a[1] = 1 * obj.num2.value;
    a[2] = 1 * obj.num3.value;
    a[3] = 1 * obj.num4.value;
    a[4] = 1 * obj.num5.value;
    a[5] = 1 * obj.num6.value;
    var v, s = 0;
    for (var i = 0; i < 6; i++) s += a[i];
    v = s / 6;
    obj.res.value = v;
}

function ex2() {
    var d = document;
    var e = d.pm1.src;
    d.pm1.src = d.pm2.src;
    d.pm2.src = e;
}

function ex3_5(obj) {
    obj.res.value = obj.num1.value * obj.num1.value;
}

function ex6(obj) {
    obj.res.value = Math.sin(obj.num1.value);
}

function ex7(obj) {
    obj.res.value = obj.num1.value * obj.num1.value;
}